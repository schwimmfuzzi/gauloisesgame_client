/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('WelcomeCtrl', function () {
  var ctrl;

  beforeEach(module('welcome'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('WelcomeCtrl');
  }));

  it('should have ctrlName as WelcomeCtrl', function () {
    expect(ctrl.ctrlName).toEqual('WelcomeCtrl');
  });
});
