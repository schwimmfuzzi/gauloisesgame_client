(function() {
  'use strict';

  /**
   * @ngdoc object
   * @name moderator.controller:ModeratorCtrl
   *
   * @description
   *
   */
  angular
    .module('moderator')
    .controller('ModeratorCtrl', ModeratorCtrl);

  function ModeratorCtrl(Stats, Quiz, Image, Points, $log, $interval, options, $rootScope, $timeout, $state) {
    var vm = this;
    vm.ctrlName = 'ModeratorCtrl';

    var vm = this;
    var stopChecking;
    vm.ctrlName = 'BackendCtrl';
    vm.stats = {};

    vm.init = function() {
      Stats.list()
        .success(function(data) {
          vm.stats = data;
          vm.checkGameStats();
        })
        .error(function(err) {
          $log.error(err);
        });
    }

    vm.getStats = function() {
      Stats.list()
        .success(function(data) {
          vm.stats = data;
          vm.getCurrentQuiz(data.currentQuiz);
        })
        .error(function(err) {
          $log.error(err);
        });
    }

	vm.cancelGame = function () {
		var s = vm.stats;
		s.abortGame = true;
    s.pausingPlayer = 0;
		Stats.update(s)
		  .success(function(data) {
			 vm.stats = data;
       vm.pausingPlayer = data.pausingPlayer;
		  })
		  .error(function(err) {
			$log.error(err);
		  });
	};

    vm.getPoints = function() {
      Points.list()
        .success(function(data) {
          vm.points = data;
        })
        .error(function(err) {
          $log.error(err);
        });
    };

    vm.setPoints = function(flag) {
      Points.update(vm.points)
        .success(function(data) {
          vm.points = data;
          vm.getStats();
		  if(flag) {
			  // directly forward
			  vm.nextQuiz();
		  }
        })
        .error(function(err) {
          $log.error(err);
        });
    }

    vm.pauseGame = function(p) {
      var s = {
        gamePaused: true,
        pausingPlayer: p
      };
      vm.pausingPlayer = p;
      Stats.update(s)
        .success(function(data) {
          vm.stats = data;
        })
        .error(function(err) {
          $log.error(err);
        });
    };

	vm.getImage = function(data) {
		var image = {
			filename: vm.stats.path
		};

     	if(vm.stats.source == 'default') {
			Image.show(image)
			  .success(function(data) {
				vm.curImg = data;
			  })
			  .error(function(err) {
				$log.error(err);
			  });
		} else if(vm.stats.source == 'custom') {
			Image.showCustom(image)
			  .success(function(data) {
				vm.curImg = data;
			  })
			  .error(function(err) {
				$log.error(err);
			  });
		} else {
			// $log.info(data, image, vm.stats);
		}
    };


    vm.checkGameStats = function() {
      stopChecking = $interval(function() {
        vm.getPoints();
        vm.getStats();
      }, options.settings.checkTime);
    }

    vm.stopCheckLoop = function() {
      if (angular.isDefined(stopChecking)) {
        $interval.cancel(stopChecking);
        stopChecking = undefined;
      }
    };

    vm.resumeGame = function() {
      var s = {
        gamePaused: false,
        pausingPlayer: null
      };

      vm.pausingPlayer = null;

      Stats.update(s)
        .success(function(data) {
          vm.stats = data;
        })
        .error(function(err) {
          $log.error(err);
        });
    }

    vm.nextQuiz = function() {
      var s = {
        gamePaused: false,
        pausingPlayer: null,
        nextQuiz: true,
        answerFlag: vm.answerFlag
      };
      vm.pausingPlayer = null;
      Stats.update(s)
        .success(function(data) {
          vm.stats = data;
          delete vm.answerFlag;
        })
        .error(function(err) {
          $log.error(err);
        });
    };

    vm.getCurrentQuiz = function(id) {
      Quiz.show(id)
        .success(function(data) {
          vm.currentQuiz = data;
		  vm.getImage(data);
        })
        .error(function(err) {
          $log.error(err);
        });
    };

    vm.setStats = function(s) {
      Stats.update(s)
        .success(function(data) {
          vm.stats = data;
        })
        .error(function(err) {
          $log.error(err);
        });
    }

    vm.answerCorrect = function() {
      var player = 'p' + vm.stats.pausingPlayer;
      vm.answerFlag = true;
      vm.points[player]++;
      vm.setPoints(true);

      if (vm.points[player] == 3)
        $log.info(player + ' hat gewonnen');
    }

    vm.answerIncorrect = function() {
      vm.answerFlag = false;
      var s = {
        gamePaused: true,
        pausingPlayer: null,
        answerFlag: false
      };
      vm.setStats(s);

      $timeout(function() {
        delete vm.answerFlag;
        var s = {
          gamePaused: false,
          pausingPlayer: null,
          answerFlag: undefined
        };
        vm.pausingPlayer = null;
        vm.setStats(s);
      }, options.settings.falseAnswerTime);
    }

    $rootScope.$on('keypress', function(e, data) {

      if (!vm.stats.gamePaused) { // only let buzzers fire if game is running
        switch (String.fromCharCode(data)) {
          case options.buzzers.p1:
            $log.log('player one buzzed');
            vm.pauseGame(1);
            break;
          case options.buzzers.p2:
            $log.log('player two buzzed');
            vm.pauseGame(2);
            break;
          case options.buzzers.p3:
            $log.log('player three buzzed');
            vm.pauseGame(3);
            break;
          default:
            $log.log('no one buzzered');
        }
      }
    });

    vm.init();
  }
}());

//# sourceMappingURL=moderator-controller.js.map
