/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('PointsCtrl', function () {
  var ctrl;

  beforeEach(module('points'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('PointsCtrl');
  }));

  it('should have ctrlName as PointsCtrl', function () {
    expect(ctrl.ctrlName).toEqual('PointsCtrl');
  });
});
