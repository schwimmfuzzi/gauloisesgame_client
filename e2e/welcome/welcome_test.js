/* global describe, beforeEach, it, browser, expect */
'use strict';

var WelcomePagePo = require('./welcome.po');

describe('Welcome page', function () {
  var welcomePage;

  beforeEach(function () {
    welcomePage = new WelcomePagePo();
    browser.get('/#/welcome');
  });

  it('should say WelcomeCtrl', function () {
    expect(welcomePage.heading.getText()).toEqual('welcome');
    expect(welcomePage.text.getText()).toEqual('WelcomeCtrl');
  });
});
