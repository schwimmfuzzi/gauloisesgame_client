(function () {
  'use strict';

  angular
    .module('moderator')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('backend.moderator', {
        url: '/moderator',
        templateUrl: 'moderator/moderator.tpl.html',
        controller: 'ModeratorCtrl',
        controllerAs: 'moderator'
      });
  }
}());
