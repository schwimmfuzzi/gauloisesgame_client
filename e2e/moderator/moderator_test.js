/* global describe, beforeEach, it, browser, expect */
'use strict';

var ModeratorPagePo = require('./moderator.po');

describe('Moderator page', function () {
  var moderatorPage;

  beforeEach(function () {
    moderatorPage = new ModeratorPagePo();
    browser.get('/#/moderator');
  });

  it('should say ModeratorCtrl', function () {
    expect(moderatorPage.heading.getText()).toEqual('moderator');
    expect(moderatorPage.text.getText()).toEqual('ModeratorCtrl');
  });
});
