(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name game.factory:Quiz
   *
   * @description
   *
   */
  angular
    .module('game')
    .factory('Quiz', Quiz);

  function Quiz($http, options) {
    return {
      rand: function() {
        return $http.get(options.api.baseUrl + '/quizes/rand');
      },
      show: function(id) {
        return $http.get(options.api.baseUrl + '/quizes/' + id);
      },
      list: function() {
        return $http.get(options.api.baseUrl + '/quizes');
      },
      update: function(id, quiz) {
        return $http.post(options.api.baseUrl + '/quizes/' + id, {
          quiz: quiz
        });
      },
      updateAll: function(quizes) {
        return $http.post(options.api.baseUrl + '/quizes/all', {
          quizes: quizes
        });
      },
      updateCustom: function(quizList) {
        return $http.post(options.api.baseUrl + '/quizes/custom', {
          quizList: quizList
        });
      }
    };
  }
}());

//# sourceMappingURL=quiz-factory.js.map
