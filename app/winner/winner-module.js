(function () {
  'use strict';

  /* @ngdoc object
   * @name winner
   * @description
   *
   */
  angular
    .module('winner', [
      'ui.router'
    ]);
}());
