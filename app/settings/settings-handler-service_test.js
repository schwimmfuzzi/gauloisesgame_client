/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('SettingsHandler', function () {
  var service;

  beforeEach(module('gauloisesGame'));

  beforeEach(inject(function (SettingsHandler) {
    service = SettingsHandler;
  }));

  it('should equal SettingsHandler', function () {
    expect(service.get()).toEqual('SettingsHandler');
  });
});
