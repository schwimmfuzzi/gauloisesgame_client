(function () {
  'use strict';

  angular
    .module('imgmng')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('backend.imgmng', {
        url: '/imgmng',
        templateUrl: 'imgmng/imgmng.tpl.html',
        controller: 'ImgmngCtrl',
        controllerAs: 'imgmng'
      });
  }
}());

//# sourceMappingURL=imgmng-routes.js.map
