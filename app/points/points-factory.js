(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name points.factory:Points
   *
   * @description
   *
   */
  angular
    .module('points')
    .factory('Points', Points);

	function Points($http, options) {
      return {
   	 update: function (points) {
   	   return $http.patch(options.api.baseUrl + '/points', {points: points});
   	 },
   	 list: function () {
   	   return $http.get(options.api.baseUrl + '/points');
   	 }
      };
    }
   }());
