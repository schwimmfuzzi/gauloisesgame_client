(function () {
  'use strict';

  angular
    .module('winner')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('winner', {
        url: '/winner',
        templateUrl: 'winner/winner.tpl.html',
        controller: 'WinnerCtrl',
        controllerAs: 'winner'
      });
  }
}());
