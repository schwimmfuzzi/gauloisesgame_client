(function () {
  'use strict';

  /* @ngdoc object
   * @name moderator
   * @description
   *
   */
  angular
    .module('moderator', [
      'ui.router'
    ]);
}());

//# sourceMappingURL=moderator-module.js.map
