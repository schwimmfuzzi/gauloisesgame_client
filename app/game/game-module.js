(function () {
  'use strict';

  /* @ngdoc object
   * @name game
   * @description
   *
   */
  angular
    .module('game', [
      'ui.router',
	  'ngSanitize'
    ]);
}());
