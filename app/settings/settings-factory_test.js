/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Settings', function () {
  var factory;

  beforeEach(module('gauloisesGame'));

  beforeEach(inject(function (Settings) {
    factory = Settings;
  }));

  it('should have someValue be Settings', function () {
    expect(factory.someValue).toEqual('Settings');
  });

  it('should have someMethod return Settings', function () {
    expect(factory.someMethod()).toEqual('Settings');
  });
});
