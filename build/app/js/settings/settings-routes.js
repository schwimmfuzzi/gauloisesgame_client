(function () {
  'use strict';

  angular
    .module('settings')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('backend.settings', {
        url: '/settings',
        templateUrl: 'settings/settings.tpl.html',
        controller: 'SettingsCtrl',
        controllerAs: 'settings'
      });
  }
}());

//# sourceMappingURL=settings-routes.js.map
