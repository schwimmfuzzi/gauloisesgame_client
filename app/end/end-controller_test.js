/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('EndCtrl', function () {
  var ctrl;

  beforeEach(module('end'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('EndCtrl');
  }));

  it('should have ctrlName as EndCtrl', function () {
    expect(ctrl.ctrlName).toEqual('EndCtrl');
  });
});
