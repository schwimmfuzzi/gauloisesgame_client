/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Image', function () {
  var factory;

  beforeEach(module('gauloisesGame'));

  beforeEach(inject(function (Image) {
    factory = Image;
  }));

  it('should have someValue be Image', function () {
    expect(factory.someValue).toEqual('Image');
  });

  it('should have someMethod return Image', function () {
    expect(factory.someMethod()).toEqual('Image');
  });
});
