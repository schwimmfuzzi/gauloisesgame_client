/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('ModeratorCtrl', function () {
  var ctrl;

  beforeEach(module('moderator'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('ModeratorCtrl');
  }));

  it('should have ctrlName as ModeratorCtrl', function () {
    expect(ctrl.ctrlName).toEqual('ModeratorCtrl');
  });
});
