(function () {
  'use strict';

  /* @ngdoc object
   * @name game
   * @description
   *
   */
  angular
    .module('game', [
      'ui.router',
	  'ngSanitize'
    ]);
}());

//# sourceMappingURL=game-module.js.map
