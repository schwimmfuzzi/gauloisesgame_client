(function () {
  'use strict';

  /* @ngdoc object
   * @name points
   * @description
   *
   */
  angular
    .module('points', [
      'ui.router'
    ]);
}());
