(function () {
  'use strict';

  /* @ngdoc object
   * @name settings
   * @description
   *
   */
  angular
    .module('settings', [
      'ui.router'
    ]);
}());

//# sourceMappingURL=settings-module.js.map
