/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('StatHandler', function () {
  var service;

  beforeEach(module('gauloisesGame'));

  beforeEach(inject(function (StatHandler) {
    service = StatHandler;
  }));

  it('should equal StatHandler', function () {
    expect(service.get()).toEqual('StatHandler');
  });
});
