(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name gauloisesGame.service:StatHandler
   *
   * @description
   *
   */
  angular
    .module('gauloisesGame')
    .service('StatHandler', StatHandler);

  function StatHandler() {
    var self = this;

    self.get = function () {
      return 'StatHandler';
    };
  }
}());

//# sourceMappingURL=stat-handler-service.js.map
