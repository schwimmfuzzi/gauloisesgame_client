/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('BackendCtrl', function () {
  var ctrl;

  beforeEach(module('backend'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('BackendCtrl');
  }));

  it('should have ctrlName as BackendCtrl', function () {
    expect(ctrl.ctrlName).toEqual('BackendCtrl');
  });
});
