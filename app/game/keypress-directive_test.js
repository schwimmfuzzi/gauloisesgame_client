/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('keypress', function () {
  var scope
    , element;

  beforeEach(module('game', 'game/keypress-directive.tpl.html'));

  beforeEach(inject(function ($compile, $rootScope) {
    scope = $rootScope.$new();
    element = $compile(angular.element('<keypress></keypress>'))(scope);
  }));

  it('should have correct text', function () {
    scope.$apply();
    expect(element.isolateScope().keypress.name).toEqual('keypress');
  });
});
