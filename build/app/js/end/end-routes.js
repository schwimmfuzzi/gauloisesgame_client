(function () {
  'use strict';

  angular
    .module('end')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('end', {
        url: '/end',
        templateUrl: 'end/end.tpl.html',
        controller: 'EndCtrl',
        controllerAs: 'end'
      });
  }
}());

//# sourceMappingURL=end-routes.js.map
