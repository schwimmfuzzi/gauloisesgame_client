(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name game.directive:keypress
   * @restrict EA
   * @element
   *
   * @description
   *
   * @example
     <example module="game">
       <file name="index.html">
        <keypress></keypress>
       </file>
     </example>
   *
   */
  angular
    .module('game')
    .directive('keypress', keypress);

  function keypress($document, $rootScope, $state) {
    return {
      restrict: 'EA',
      scope: {},
      templateUrl: 'game/keypress-directive.tpl.html',
      replace: false,
      controllerAs: 'keypress',
      controller: function () {
        var vm = this;
        vm.name = 'keypress';
      },
      link: function (scope, element, attrs) {
        /* jshint unused:false */
        /* eslint "no-unused-vars": [2, {"args": "none"}] */
		$document.bind('keyup', function(e) {
			// if($state.is('game')) {
	          $rootScope.$broadcast('keypress', e.which);
			// }
        });
      }
    };
  }
}());

//# sourceMappingURL=keypress-directive.js.map
