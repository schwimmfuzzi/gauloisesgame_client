/* global describe, beforeEach, it, browser, expect */
'use strict';

var WinnerPagePo = require('./winner.po');

describe('Winner page', function () {
  var winnerPage;

  beforeEach(function () {
    winnerPage = new WinnerPagePo();
    browser.get('/#/winner');
  });

  it('should say WinnerCtrl', function () {
    expect(winnerPage.heading.getText()).toEqual('winner');
    expect(winnerPage.text.getText()).toEqual('WinnerCtrl');
  });
});
