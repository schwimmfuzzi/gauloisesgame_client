(function () {
  'use strict';

  angular
    .module('points')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('points', {
        url: '/points',
        templateUrl: 'points/points.tpl.html',
        controller: 'PointsCtrl',
        controllerAs: 'points'
      });
  }
}());
