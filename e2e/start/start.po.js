/* global element, by */
'use strict';

function StartPage() {
  this.text = element(by.tagName('p'));
  this.heading = element(by.tagName('h2'));
}

module.exports = StartPage;
