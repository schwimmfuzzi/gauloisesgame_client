/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('options', function () {
  var constant;

  beforeEach(module('gauloisesGame'));

  beforeEach(inject(function (options) {
    constant = options;
  }));

  it('should equal 0', function () {
    expect(constant).toBe(0);
  });
});
