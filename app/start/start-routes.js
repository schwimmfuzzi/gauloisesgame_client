(function () {
  'use strict';

  angular
    .module('start')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('start', {
        url: '/start',
        templateUrl: 'start/start.tpl.html',
        controller: 'StartCtrl',
        controllerAs: 'start'
      });
  }
}());
