(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name gauloisesGame.factory:Socket
   *
   * @description
   *
   */
  angular
    .module('gauloisesGame')
    .factory('Socket', Socket);

  function Socket() {
    var socket = io.connect();

    return {
      on: function(eventName, callback) {
        socket.on(eventName, callback);
      },
      emit: function(eventName, data) {
        socket.emit(eventName, data);
      }
    };
  }
}());

//# sourceMappingURL=socket-factory.js.map
