(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name winner.controller:WinnerCtrl
   *
   * @description
   *
   */
  angular
    .module('winner')
    .controller('WinnerCtrl', WinnerCtrl);

  function WinnerCtrl($log, Points, Stats, Sounds, $timeout, options, $state, SettingsHandler) {
    var vm = this;
    vm.ctrlName = 'WinnerCtrl';

	vm.init = function () {
		vm.getPoints();
		console.log('resetting game now');
		vm.settings = SettingsHandler.getSettings();
		$log.log(vm.settings);
	};

	vm.getTheWinner = function() {
      vm.winner = '';
      if (vm.points.p1 >= vm.settings.winningPoints)
        vm.winner = 1;
      else if (vm.points.p2 >=  vm.settings.winningPoints)
        vm.winner = 2;
      else if (vm.points.p3 >=  vm.settings.winningPoints)
        vm.winner = 3;

		vm.celebrate();

		vm.setStats();
    };

	vm.celebrate = function () {
		Sounds.play('celebrate');
      $timeout(function() {
		  $state.go('end');
      }, options.settings.celebrateTime);
	};

	vm.getStats = function () {
		Stats.list()
		.success(function (data) {
			$log.log(data);
		})
		.error(function (err) {
			$log.error(err);
		});
	};



	vm.setStats = function () {
		Stats.update({
			gamePaused: true,
			nextQuiz: false
		})
		.success(function (data) {
			$log.log(data);
		})
		.error(function (err) {
			$log.error(err);
		});
	};


	vm.getPoints = function() {
      Points.list()
        .success(function(data) {
          vm.points = data;
		  vm.getTheWinner();
        })
        .error(function(err) {
          $log.error(err);
        });
    };

	vm.init();
  }
}());
