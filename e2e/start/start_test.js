/* global describe, beforeEach, it, browser, expect */
'use strict';

var StartPagePo = require('./start.po');

describe('Start page', function () {
  var startPage;

  beforeEach(function () {
    startPage = new StartPagePo();
    browser.get('/#/start');
  });

  it('should say StartCtrl', function () {
    expect(startPage.heading.getText()).toEqual('start');
    expect(startPage.text.getText()).toEqual('StartCtrl');
  });
});
