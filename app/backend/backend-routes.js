(function () {
  'use strict';

  angular
    .module('backend')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('backend', {
        url: '/backend',
        templateUrl: 'backend/backend.tpl.html',
        controller: 'BackendCtrl',
        controllerAs: 'backend'
      });
  }
}());
