(function () {
  'use strict';

  /* @ngdoc object
   * @name end
   * @description
   *
   */
  angular
    .module('end', [
      'ui.router'
    ]);
}());

//# sourceMappingURL=end-module.js.map
