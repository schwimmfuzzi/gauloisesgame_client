(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name gauloisesGame.service:SettingsHandler
   *
   * @description
   *
   */
  angular
    .module('gauloisesGame')
    .service('SettingsHandler', SettingsHandler);

  function SettingsHandler($log, options, Settings) {
    var self = this;

    self.get = function() {
      return 'SettingsHandler';
    };

    self.init = function() {
      self.loadSettings();
    };

    self.setSettings = function(s) {
    //   $log.log('update settings', s);
      self.settings = s;
    };

    self.getSettings = function() {
    //   $log.info('settings in handler ', self.settings);
     return self.settings || options.settings;
    }

    self.loadSettings = function() {
     return Settings.list()
        .success(function(data) {
          self.setSettings(data);
        })
        .error(function(data) {
          $log.error(data);
        })
    }

    self.init();
  }
}());
