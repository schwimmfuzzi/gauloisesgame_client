(function () {
  'use strict';

  /* @ngdoc object
   * @name points
   * @description
   *
   */
  angular
    .module('points', [
      'ui.router'
    ]);
}());

//# sourceMappingURL=points-module.js.map
