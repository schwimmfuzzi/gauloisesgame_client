/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('WinnerCtrl', function () {
  var ctrl;

  beforeEach(module('winner'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('WinnerCtrl');
  }));

  it('should have ctrlName as WinnerCtrl', function () {
    expect(ctrl.ctrlName).toEqual('WinnerCtrl');
  });
});
