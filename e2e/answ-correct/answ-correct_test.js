/* global describe, beforeEach, it, browser, expect */
'use strict';

var AnswCorrectPagePo = require('./answ-correct.po');

describe('Answ correct page', function () {
  var answCorrectPage;

  beforeEach(function () {
    answCorrectPage = new AnswCorrectPagePo();
    browser.get('/#/answ-correct');
  });

  it('should say AnswCorrectCtrl', function () {
    expect(answCorrectPage.heading.getText()).toEqual('answCorrect');
    expect(answCorrectPage.text.getText()).toEqual('AnswCorrectCtrl');
  });
});
