/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Points', function () {
  var factory;

  beforeEach(module('points'));

  beforeEach(inject(function (Points) {
    factory = Points;
  }));

  it('should have someValue be Points', function () {
    expect(factory.someValue).toEqual('Points');
  });

  it('should have someMethod return Points', function () {
    expect(factory.someMethod()).toEqual('Points');
  });
});
