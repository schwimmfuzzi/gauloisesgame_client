(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name points.controller:PointsCtrl
   *
   * @description
   *
   */
  angular
    .module('points')
    .controller('PointsCtrl', PointsCtrl);

  function PointsCtrl() {
    var vm = this;
    vm.ctrlName = 'PointsCtrl';
  }
}());

//# sourceMappingURL=points-controller.js.map
