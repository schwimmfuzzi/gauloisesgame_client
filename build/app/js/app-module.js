(function () {
  'use strict';

  /* @ngdoc object
   * @name gauloisesGame
   * @description
   *
   */
  angular
    .module('gauloisesGame', [
      'ui.router',
      'ui.bootstrap',
	  'ngAudio',
	  'toggle-switch',
	  'start',
      'game',
      'winner',
      'end',
      'welcome',
      'backend',
      'points',
      'answCorrect',
      'settings',
      'moderator',
      'imgmng'
    ]);
}());

//# sourceMappingURL=app-module.js.map
