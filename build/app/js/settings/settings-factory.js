(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name gauloisesGame.factory:Settings
   *
   * @description
   *
   */
  angular
    .module('gauloisesGame')
    .factory('Settings', Settings);

  function Settings($http, options) {
    return {
      update: function(settings) {
        return $http.patch(options.api.baseUrl + '/settings', {
          settings: settings
        });
      },
      list: function() {
        return $http.get(options.api.baseUrl + '/settings');
      }
    };
  }
}());

//# sourceMappingURL=settings-factory.js.map
