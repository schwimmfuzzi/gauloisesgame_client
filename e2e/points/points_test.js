/* global describe, beforeEach, it, browser, expect */
'use strict';

var PointsPagePo = require('./points.po');

describe('Points page', function () {
  var pointsPage;

  beforeEach(function () {
    pointsPage = new PointsPagePo();
    browser.get('/#/points');
  });

  it('should say PointsCtrl', function () {
    expect(pointsPage.heading.getText()).toEqual('points');
    expect(pointsPage.text.getText()).toEqual('PointsCtrl');
  });
});
