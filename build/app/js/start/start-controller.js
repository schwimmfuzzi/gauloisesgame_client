(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name start.controller:StartCtrl
   *
   * @description
   *
   */
  angular
    .module('start')
    .controller('StartCtrl', StartCtrl);

  function StartCtrl($log, Settings, SettingsHandler, Stats, $timeout, options, $state) {
    var vm = this;
    vm.ctrlName = 'StartCtrl';

	vm.init = function () {
		Settings.list()
		.success(function (data) {
			$log.log('i got ...');
			vm.setStats({abortGame: false});
			SettingsHandler.setSettings(data);
		})
		.error(function (err) {
			$log.error(err);
		});
	};

  vm.deferredForward = function () {
    $timeout(function () {
      $state.go('game');
    }, options.settings.deferWelcomeTime);
  };

	vm.setStats =function (s) {
		Stats.update(s)
		.success(function(data){
			$log.log(data);
		})
		.error(function(err){
			$log.error(err);
		});
	}

	vm.init();
  }
}());

//# sourceMappingURL=start-controller.js.map
