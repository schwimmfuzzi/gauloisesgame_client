(function () {
  'use strict';

  /* @ngdoc object
   * @name welcome
   * @description
   *
   */
  angular
    .module('welcome', [
      'ui.router'
    ]);
}());

//# sourceMappingURL=welcome-module.js.map
