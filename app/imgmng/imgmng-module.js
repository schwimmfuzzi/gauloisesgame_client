(function () {
  'use strict';

  /* @ngdoc object
   * @name imgmng
   * @description
   *
   */
  angular
    .module('imgmng', [
      'ui.router'
    ]);
}());
