(function() {
  'use strict';

  /**
   * @ngdoc service
   * @name gauloisesGame.service:Sounds
   *
   * @description
   *
   */
  angular
    .module('gauloisesGame')
    .service('Sounds', Sounds);

  function Sounds(ngAudio, $log, $timeout, options) {
    var self = this;


    self.get = function() {
      return 'Sounds';
    };

    self.init = function() {
      self.loadSounds();
      self.currentlyPlaying = false;
      self.time = {
        answRight: options.settings.rightAnswerTime,
        answFalse: options.settings.falseAnswerTime,
        celebrate: options.settings.winnerTime,
      }
    };

    self.loadSounds = function() {
      $log.log('sounds loaded');
      self.sounds = {
        answRight: ngAudio.load('../sounds/rightAnswer.mp3'),
        answFalse: ngAudio.load('../sounds/wrongAnswer.mp3'),
        celebrate: ngAudio.load('../sounds/winner.mp3')
      }
    };

    self.play = function(item) {

      if (!self.currentlyPlaying) {
        self.currentlyPlaying = true;
        // $log.log('false sound');
        self.sounds[item].play();

        $timeout(function() {
          self.currentlyPlaying = false;
	  }, self.time[item]);
      }
    };


    self.init();
  }
}());
