(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name gauloisesGame.factory:Image
   *
   * @description
   *
   */
  angular
    .module('gauloisesGame')
    .factory('Image', Image);

	function Image($http, options) {
        return {
            show: function (image) {
              return $http.post(options.api.baseUrl + '/images/single' , {image: image});
            },
	            showCustom: function (image) {
	              return $http.post(options.api.baseUrl + '/images/single/custom' , {image: image});
	            },
          list: function () {
            return $http.get(options.api.baseUrl + '/images');
          },
		listCustom: function () {
		  return $http.get(options.api.baseUrl + '/images/custom');
		}
        };
      }
    }());

//# sourceMappingURL=image-factory.js.map
