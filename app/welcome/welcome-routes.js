(function () {
  'use strict';

  angular
    .module('welcome')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('welcome', {
        url: '/welcome',
        templateUrl: 'welcome/welcome.tpl.html',
        controller: 'WelcomeCtrl',
        controllerAs: 'welcome'
      });
  }
}());
