(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name end.controller:EndCtrl
   *
   * @description
   *
   */
  angular
    .module('end')
    .controller('EndCtrl', EndCtrl);

  function EndCtrl(Stats, Points, $log, $timeout, $state, options) {
    var vm = this;
    vm.ctrlName = 'EndCtrl';

	vm.init = function () {

			vm.setStats();
			vm.setPoints();
      vm.deferredForward();
	};

	vm.setStats = function () {
		Stats.update({
			gamePaused: false,
			nextQuiz: false
		})
		.success(function (data) {
			$log.log(data);
		})
		.error(function (err) {
			$log.error(err);
		});
	};


    vm.deferredForward = function () {
      $timeout(function () {
        $state.go('welcome');
      }, options.settings.deferEndTime);
    };

	vm.setPoints = function () {
		Points.update({
			p1: 0,
			p2: 0,
			p3: 0
		})
		.success(function (data) {
			$log.log(data);
		})
		.error(function (err) {
			$log.error(err);
		});
	};


vm.init();

  }
}());

//# sourceMappingURL=end-controller.js.map
