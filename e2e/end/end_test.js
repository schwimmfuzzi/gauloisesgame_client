/* global describe, beforeEach, it, browser, expect */
'use strict';

var EndPagePo = require('./end.po');

describe('End page', function () {
  var endPage;

  beforeEach(function () {
    endPage = new EndPagePo();
    browser.get('/#/end');
  });

  it('should say EndCtrl', function () {
    expect(endPage.heading.getText()).toEqual('end');
    expect(endPage.text.getText()).toEqual('EndCtrl');
  });
});
