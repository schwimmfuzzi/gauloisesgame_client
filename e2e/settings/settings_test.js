/* global describe, beforeEach, it, browser, expect */
'use strict';

var SettingsPagePo = require('./settings.po');

describe('Settings page', function () {
  var settingsPage;

  beforeEach(function () {
    settingsPage = new SettingsPagePo();
    browser.get('/#/settings');
  });

  it('should say SettingsCtrl', function () {
    expect(settingsPage.heading.getText()).toEqual('settings');
    expect(settingsPage.text.getText()).toEqual('SettingsCtrl');
  });
});
