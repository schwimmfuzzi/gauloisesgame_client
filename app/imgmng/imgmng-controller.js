(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name imgmng.controller:ImgmngCtrl
   *
   * @description
   *
   */
  angular
    .module('imgmng')
    .controller('ImgmngCtrl', ImgmngCtrl);

  function ImgmngCtrl(Image, $log, Quiz) {
    var vm = this;
    vm.ctrlName = 'ImgmngCtrl';

	vm.init = function () {
		vm.getImageList();
		vm.getQuizList();
		vm.imageList = [];
		vm.customImageList = [];
		vm.customList = [];
		vm.getCustomImageList();
	};


	vm.getImageForList = function (image, index) {
		var image = {
			filename: image.path
		};

		Image.show(image)
			.success(function (data) {
				vm.imageList[index] = data;
			})
			.error(function (err) {
				$log.error(err);
			});
	};
	vm.getImageForCustomList = function (image, index) {
		var image = {
			filename: image
		};
		Image.showCustom(image)
			.success(function (data) {
				vm.customImageList[index] = data;
			})
			.error(function (err) {
				$log.error(err);
			});
	};

	vm.getImageList = function () {

		Image.list()
			.success(function (data) {
				// $log.log(data);
			})
			.error(function (err) {
				$log.error(err);
			});
	};

	vm.getCustomImageList = function () {

		Image.listCustom()
			.success(function (data) {
				$log.log('custom: ', data);
				// vm.customList = data;

				angular.forEach(data, function (ci) {
					vm.customList.push(ci);
				});
				vm.processCustomQuizList(vm.customList);
			})
			.error(function (err) {
				$log.error(err);
			});
	};

	vm.updateQuiz = function (id, quiz) {

		Quiz.update(id, quiz)
			.success(function (data) {
				$log.log(data);
			})
			.error(function (err) {
				$log.error(err);
			});
	};

	vm.updateAllQuizes = function () {

		Quiz.updateAll(vm.quizList)
			.success(function (data) {
				$log.log(data);
			})
			.error(function (err) {
				$log.error(err);
			});
	};

	vm.updateCustom = function () {
			Quiz.updateCustom(vm.customList)
			.success(function (data) {
				$log.log('updated custom', data);
			})
			.error(function (err) {
				$log.error(err);
			});
	};

  vm.toggleAllQuizes = function (val) {
    if(vm.quizList) {
      angular.forEach(vm.quizList, function (quiz, id) {
        quiz.active = val;
      });
      vm.updateAllQuizes();
    }
  };

	vm.getQuizList = function (filename) {

		Quiz.list()
			.success(function (data) {
				vm.quizList = data;
				vm.processQuizList(data);
			})
			.error(function (err) {
				$log.error(err);
			});
	};

	vm.processQuizList = function (list) {
			angular.forEach(list, function (l, key) {
				vm.getImageForList(l, key);
			});
	};

	vm.processCustomQuizList = function (list) {
			angular.forEach(list, function (l, key) {
				vm.getImageForCustomList(l.path, key);
			});
	};

	vm.init();
  }
}());
