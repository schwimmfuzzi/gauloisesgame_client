(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name gauloisesGame.constant:options
   *
   * @description
   *
   */
  angular
    .module('gauloisesGame')
    .constant('options', {
		api: {
			baseUrl: 'http://192.168.178.34:3001/v1'
		},
		buzzers: {
			p1: 'A',
			p2: 'S',
			p3: 'D'
		},
		settings: {
			fadeTime: 1500,
			fadeAmount: 2,
			checkTime: 200,
			rightAnswerTime: 1800,
			falseAnswerTime: 2000,
			falseAnswerSound: 1000,
			winnerTime: 2000,
			celebrateTime: 16000,
      deferWelcomeTime: 3000,
      deferEndTime: 5000,
      deferFirstCharFade: 5000
		}
	});
}());
