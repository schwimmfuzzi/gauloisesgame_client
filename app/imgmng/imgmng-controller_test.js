/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('ImgmngCtrl', function () {
  var ctrl;

  beforeEach(module('imgmng'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('ImgmngCtrl');
  }));

  it('should have ctrlName as ImgmngCtrl', function () {
    expect(ctrl.ctrlName).toEqual('ImgmngCtrl');
  });
});
