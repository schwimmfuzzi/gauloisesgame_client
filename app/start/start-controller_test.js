/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('StartCtrl', function () {
  var ctrl;

  beforeEach(module('start'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('StartCtrl');
  }));

  it('should have ctrlName as StartCtrl', function () {
    expect(ctrl.ctrlName).toEqual('StartCtrl');
  });
});
