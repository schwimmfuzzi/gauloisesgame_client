/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Quiz', function () {
  var factory;

  beforeEach(module('game'));

  beforeEach(inject(function (Quiz) {
    factory = Quiz;
  }));

  it('should have someValue be Quiz', function () {
    expect(factory.someValue).toEqual('Quiz');
  });

  it('should have someMethod return Quiz', function () {
    expect(factory.someMethod()).toEqual('Quiz');
  });
});
