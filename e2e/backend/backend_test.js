/* global describe, beforeEach, it, browser, expect */
'use strict';

var BackendPagePo = require('./backend.po');

describe('Backend page', function () {
  var backendPage;

  beforeEach(function () {
    backendPage = new BackendPagePo();
    browser.get('/#/backend');
  });

  it('should say BackendCtrl', function () {
    expect(backendPage.heading.getText()).toEqual('backend');
    expect(backendPage.text.getText()).toEqual('BackendCtrl');
  });
});
