(function () {
  'use strict';

  /**
   * @ngdoc service
   * @name game.factory:Stats
   *
   * @description
   *
   */
  angular
    .module('game')
    .factory('Stats', Stats);

	function Stats($http, options) {
  	return {
  	  update: function (s) {
  		return $http.patch(options.api.baseUrl + '/stats', {stats: s});
  	  },
  	  list: function () {
  		return $http.get(options.api.baseUrl + '/stats');
  	  }
  	};
    }
  }());

//# sourceMappingURL=stats-factory.js.map
