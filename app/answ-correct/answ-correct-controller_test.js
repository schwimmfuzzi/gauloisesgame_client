/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('AnswCorrectCtrl', function () {
  var ctrl;

  beforeEach(module('answCorrect'));

  beforeEach(inject(function ($rootScope, $controller) {
    ctrl = $controller('AnswCorrectCtrl');
  }));

  it('should have ctrlName as AnswCorrectCtrl', function () {
    expect(ctrl.ctrlName).toEqual('AnswCorrectCtrl');
  });
});
