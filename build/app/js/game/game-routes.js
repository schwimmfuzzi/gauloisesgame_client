(function () {
  'use strict';

  angular
    .module('game')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('game', {
        url: '/game',
        templateUrl: 'game/game.tpl.html',
        controller: 'GameCtrl',
        controllerAs: 'game'
      });
  }
}());

//# sourceMappingURL=game-routes.js.map
