/* global describe, beforeEach, it, browser, expect */
'use strict';

var ImagesPagePo = require('./images.po');

describe('Images page', function () {
  var imagesPage;

  beforeEach(function () {
    imagesPage = new ImagesPagePo();
    browser.get('/#/images');
  });

  it('should say ImagesCtrl', function () {
    expect(imagesPage.heading.getText()).toEqual('images');
    expect(imagesPage.text.getText()).toEqual('ImagesCtrl');
  });
});
