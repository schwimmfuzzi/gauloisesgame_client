(function() {
  'use strict';

  /**
   * @ngdoc object
   * @name settings.controller:SettingsCtrl
   *
   * @description
   *
   */
  angular
    .module('settings')
    .controller('SettingsCtrl', SettingsCtrl);

  function SettingsCtrl(SettingsHandler, Settings, $log, $timeout) {
    var vm = this;
    vm.ctrlName = 'SettingsCtrl';


    vm.init = function() {
      vm.getSettings();
	  vm.saved = false;
    };

    vm.getSettings = function() {
      SettingsHandler.loadSettings()
	  .success(function (data) {
		  vm.data = data;
	  })
	  .error(function (err) {
		  $log.error(err);
	  });
    };


    vm.saveSettings = function() {
      Settings.update(vm.data)
        .success(function(data) {
          $log.log('saved settings', data);
          vm.data = data;
		  vm.saved = true;
		  $timeout(function() {
			  vm.saved = false;
		  }, 2500);
        })
        .error(function(err) {
          $log.error(err);
        });
    }

    vm.init();

  }
}());

//# sourceMappingURL=settings-controller.js.map
