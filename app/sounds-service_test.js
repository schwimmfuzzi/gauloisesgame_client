/* global describe, beforeEach, it, expect, inject, module */
'use strict';

describe('Sounds', function () {
  var service;

  beforeEach(module('gauloisesGame'));

  beforeEach(inject(function (Sounds) {
    service = Sounds;
  }));

  it('should equal Sounds', function () {
    expect(service.get()).toEqual('Sounds');
  });
});
