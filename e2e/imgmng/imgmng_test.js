/* global describe, beforeEach, it, browser, expect */
'use strict';

var ImgmngPagePo = require('./imgmng.po');

describe('Imgmng page', function () {
  var imgmngPage;

  beforeEach(function () {
    imgmngPage = new ImgmngPagePo();
    browser.get('/#/imgmng');
  });

  it('should say ImgmngCtrl', function () {
    expect(imgmngPage.heading.getText()).toEqual('imgmng');
    expect(imgmngPage.text.getText()).toEqual('ImgmngCtrl');
  });
});
