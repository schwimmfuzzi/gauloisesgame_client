(function () {
  'use strict';

  /* @ngdoc object
   * @name start
   * @description
   *
   */
  angular
    .module('start', [
      'ui.router'
    ]);
}());

//# sourceMappingURL=start-module.js.map
