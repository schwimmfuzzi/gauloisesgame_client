(function() {
  'use strict';

  /**
   * @ngdoc object
   * @name answCorrect.controller:AnswCorrectCtrl
   *
   * @description
   *
   */
  angular
    .module('answCorrect')
    .controller('AnswCorrectCtrl', AnswCorrectCtrl);

  function AnswCorrectCtrl($state, $timeout, options, Points, $log, Sounds, SettingsHandler) {
    var vm = this;
    vm.ctrlName = 'AnswCorrectCtrl';
    var ctr = 0;
    vm.init = function() {

      vm.settings = SettingsHandler.getSettings();
      $log.log(vm.settings);

      Sounds.play('answRight');
      vm.getPoints();
      $timeout(function() {
        if (vm.isThereAWinner()) {
          ctr++;
          $log.log(ctr);
          $state.go('winner');
        } else {
          $state.go('game');
        }
      }, options.settings.rightAnswerTime);
    };

    vm.isThereAWinner = function() {
      var flag = false;
      if (vm.points.p1 >= vm.settings.winningPoints)
        flag = true;
      else if (vm.points.p2 >= vm.settings.winningPoints)
        flag = true;
      else if (vm.points.p3 >= vm.settings.winningPoints)
        flag = true;

      return flag;
    };

    vm.getPoints = function() {
      Points.list()
        .success(function(data) {
          vm.points = data;
        })
        .error(function(err) {
          $log.error(err);
        });
    };

    vm.init();
  }
}());

//# sourceMappingURL=answ-correct-controller.js.map
