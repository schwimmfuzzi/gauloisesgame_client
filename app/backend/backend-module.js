(function () {
  'use strict';

  /* @ngdoc object
   * @name backend
   * @description
   *
   */
  angular
    .module('backend', [
      'ui.router'
    ]);
}());
