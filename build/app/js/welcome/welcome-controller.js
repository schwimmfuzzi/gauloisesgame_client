(function () {
  'use strict';

  /**
   * @ngdoc object
   * @name welcome.controller:WelcomeCtrl
   *
   * @description
   *
   */
  angular
    .module('welcome')
    .controller('WelcomeCtrl', WelcomeCtrl);

  function WelcomeCtrl() {
    var vm = this;
    vm.ctrlName = 'WelcomeCtrl';
  }
}());

//# sourceMappingURL=welcome-controller.js.map
