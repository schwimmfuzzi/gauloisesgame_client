(function () {
  'use strict';

  angular
    .module('gauloisesGame')
    .config(config);

  function config($urlRouterProvider) {
    $urlRouterProvider.otherwise('/welcome');
  }
}());
