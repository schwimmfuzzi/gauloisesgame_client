(function () {
  'use strict';

  angular
    .module('answCorrect')
    .config(config);

  function config($stateProvider) {
    $stateProvider
      .state('answcorrect', {
        url: '/answ-correct',
        templateUrl: 'answ-correct/answ-correct.tpl.html',
        controller: 'AnswCorrectCtrl',
        controllerAs: 'answCorrect'
      });
  }
}());
