(function() {
  'use strict';

  /**
   * @ngdoc object
   * @name game.controller:GameCtrl
   *
   * @description
   *
   */
  angular
    .module('game')
    .controller('GameCtrl', GameCtrl);

  function GameCtrl(Quiz, Stats, Points, Image, $log, $rootScope, options, $interval, $state, $timeout, Sounds, SettingsHandler) {
    var vm = this;
    var stop, stopChecking, stopPauseChecking;

    vm.ctrlName = 'GameCtrl';
    vm.extractedChars = [];
    vm.charLoopCtr = 0;

    vm.dynOptions = {};


    vm.init = function() {
      SettingsHandler.loadSettings()
        .success(function(data) {
          vm.dynOptions = data;
        })
        .error(function(err) {
          $log.error(err);
        });

        vm.getRandomQuiz();

      //   vm.checkIfContinued();
    };

    vm.fadeInCharsLoop = function() {
      if (angular.isDefined(stop)) return;

      stop = $interval(function() {
        if (!vm.stats.gamePaused && !vm.hitEmptyFlag) {

          for (var i = 0; i < vm.dynOptions.fadeAmount; i++) {
            vm.fadeInRandomChar(vm.charLoopCtr);
          }

        } else {
          //   $log.log('game is paused...');
          vm.stopFadeLoop();
        }
	}, vm.dynOptions.fadeTime, vm.lengthWithoutSpaces);
    };

    vm.setStats = function(s) {
      Stats.update(s)
        .success(function(data) {
          $log.log('stats updated');
        })
        .error(function(err) {
          $log.error(err);
        });
    }


    vm.fadeInRandomChar = function(ctr) {
      if (ctr >= vm.lengthWithoutSpaces) {
        // $log.log('no more chars left!');
        vm.stopFadeLoop();
        // $log.log('empty......');
		vm.hitEmptyFlag = true;
        vm.stopCheckLoop();
        // vm.stopPausedCheckLoop();
      } else {

        var tmpIdx = vm.random(0, vm.extractedChars.length);
        if (!vm.extractedChars[tmpIdx].show) {
          vm.extractedChars[tmpIdx].show = true;
          vm.charLoopCtr++;
        } else {

          vm.fadeInRandomChar(vm.charLoopCtr);
        }

      }
    };

    vm.random = function(low, high) {
      return Math.floor(Math.random() * (high - low) + low);
    }

    vm.stopFadeLoop = function() {
      if (angular.isDefined(stop)) {
        $interval.cancel(stop);
        stop = undefined;
      }
    };

    vm.getRandomQuiz = function() {
      Quiz.rand()
        .success(function(data) {
          vm.curQuiz = data;
          vm.extractChars(data.title);
          vm.initGame();
          vm.setStats({
            source: data.source,
            path: data.path,
            title: data.title
          });
          vm.getImage(data);

        })
        .error(function(err) {
          $log.error(err);
        });
    };

    vm.getImage = function(data) {
    //   console.log(data);
      var image = {
        filename: data.path
      };

      if (data.source == 'default') {
        Image.show(image)
          .success(function(data) {
            vm.curImg = data;
          })
          .error(function(err) {
            $log.error(err);
          });
      } else {
        Image.showCustom(image)
          .success(function(data) {
            vm.curImg = data;
          })
          .error(function(err) {
            $log.error(err);
          });
      }
    };

    vm.checkIfContinued = function() {
      //   $log.log('c');
      if (angular.isDefined(stopChecking)) return;

      stopChecking = $interval(function() {
        if (vm.stats.gamePaused) {
          Stats.list()
            .success(function(data) {
              vm.stats = data;
            //   $log.info(data);
              if (vm.stats.answerFlag === true) {
                vm.stopCheckLoop();
				vm.stopFadeLoop();
				vm.stopPausedCheckLoop();
                $state.go('answcorrect');
              } else if (vm.stats.abortGame) {
                vm.clearPoints();
                $state.go('welcome');
              } else if (vm.stats.nextQuiz) {
                $state.reload();
              }
              vm.addFalseAnswerClazz = (vm.stats.answerFlag === false) ? true : false;
            })
            .error(function(err) {
              $log.error(err);
            });
        } else {
          //   $log.log('game is continued...');
          vm.fadeInCharsLoop();
          //
          vm.checkIfPaused();
        }
      }, options.settings.checkTime);

    };

    vm.stopCheckLoop = function() {
      if (angular.isDefined(stopChecking)) {
        $interval.cancel(stopChecking);
        stopChecking = undefined;
      }
    };

    // vm.animateFalseAnswer = function() {
    //   vm.addFalseAnswerClazz = true;
    //   Sounds.playFalse();
    //   $timeout(function() {
    //     vm.addFalseAnswerClazz = false;
    //   }, options.falseAnswerTime);
    // }


    vm.checkIfPaused = function() {
      //   $log.log('cp');
      if (angular.isDefined(stopPauseChecking)) return;

      stopPauseChecking = $interval(function() {
        Stats.list()
          .success(function(data) {
            vm.stats = data;

            if (vm.stats.answerFlag === true) {
              vm.stopPausedCheckLoop();
              $state.go('answcorrect');
            } else if (vm.stats.abortGame) {
              vm.clearPoints();
              $state.go('welcome');
            } else if (vm.stats.nextQuiz) {
				vm.stopCheckLoop();
			 vm.stopFadeLoop();
              $state.reload();
            }
            vm.addFalseAnswerClazz = (vm.stats.answerFlag === false) ? true : false;
            if (vm.addFalseAnswerClazz)
              Sounds.play('answFalse');

            if (vm.stats.gamePaused) {} else {
              //   $log.log('game is continued...');
              vm.stopPausedCheckLoop();
              vm.checkIfContinued();
            }
          })
          .error(function(err) {
            $log.error(err);
          });

      }, options.settings.checkTime);

    };

    vm.stopPausedCheckLoop = function() {
      if (angular.isDefined(stopPauseChecking)) {
        $interval.cancel(stopPauseChecking);
        stopPauseChecking = undefined;
      }
    };


    vm.extractChars = function(title) {
      vm.extractedChars = [];
      vm.numOfSpaces = (title.split(" ").length - 1);

      vm.lengthWithoutSpaces = title.length - vm.numOfSpaces;

      angular.forEach(title, function(c) {
        vm.extractedChars.push({
          code: (c === ' ') ? '&nbsp;' : c, // use blank char for space
          clazz: (c === ' ') ? 'spacer' : '', // use class for spaces
          show: (c === ' ') ? true : false // display spaces directly
        });
      });

      vm.calcCssClass();
    };

    vm.getPoints = function() {
      Points.list()
        .success(function(data) {
          vm.points = data;
        })
        .error(function(err) {
          $log.error(err);
        });
    };

    vm.calcCssClass = function() {
      if (vm.extractedChars.length > 16) {
        var factor = 16 / vm.extractedChars.length;
        // $log.log(factor);
        vm.titleFontSize = 6 * factor;
        vm.titleFontWidth = 90 * factor;
        vm.titleFontHeight = 120 * factor;
      }
    };

    vm.clearPoints = function () {
      Points.update({
  			p1: 0,
  			p2: 0,
  			p3: 0
  		})
  		.success(function (data) {
  			// $log.log(data);
  		})
  		.error(function (err) {
  			$log.error(err);
  		});
    }


    vm.initGame = function() {
      vm.getPoints();
      Stats.list()
        .success(function(data) {
          vm.stats = data;
          $timeout(function () {
            vm.fadeInCharsLoop();
            vm.checkIfPaused();
            console.log('hit timeout');
          }, SettingsHandler.settings.fadeDefer);

        })
        .error(function(err) {
          $log.error(err);
        });
    };

    vm.init();
  }
}());

//# sourceMappingURL=game-controller.js.map
